//Future use
/** imoveis indexes **/
db.getCollection("imoveis").ensureIndex({
    "_id": NumberInt(1)
}, [

]);

/** imoveis records **/
db.getCollection("imoveis").insert({
    "_id": ObjectId("5899bb609835dae51a000001"),
    "autoIndexId": "true",
    "disponibilidade": "01/01/2017",
    "descricao": "SOBRADO BEM LOCALIZADO NA VILA GUARANI PRÓXIMO A AVENIDA BARÃO DE MAUÁ, DOCUMENTOS OK PARA FINANCIAMENTO. CONSULTE NOSSOS CORRETORES!!!",
    "email": "email@email.com",
    "imagem": "https://imganuncios.mitula.net/sobrado_casa_para_venda_vila_guarani_maua_8810061484093118101.jpg",
    "telefone": "(11) 998173273",
    "titulo": "Casa / Sobrado, Vila Guarani",
    "valor": "445.000,00"
});
db.getCollection("imoveis").insert({
    "_id": ObjectId("5899bab39835da89e0000002"),
    "autoIndexId": "true",
    "disponibilidade": "01/01/2017",
    "descricao": "Ótima casa para morar, amplo espaço",
    "email": "email@email.com",
    "imagem": "http://multimidia.lopes.com.br/268/214-IM61301/casa-vila-prudente-sao-paulo-foto-61301eca7257010.jpg",
    "telefone": "(11) 998173276",
    "titulo": "Casa Vila Prudente",
    "valor": "170.000,00"
});
db.getCollection("imoveis").insert({
    "_id": ObjectId("5899c2ce9835da89e0000003"),
    "autoIndexId": "true",
    "disponibilidade": "01/01/2017",
    "descricao": "Apartamento de 50 m², ótima localização, perto de shopping e metro vila Prudente",
    "email": "email @ email.com",
    "imagem": "http: //cdn1.valuegaia.com.br/watermark/agencies/11699/properties/523184332_11699700A9B427B13FB213F8B5BFFA0A649C65E518236129412.jpg",
    "telefone": "(11) 998173273",
    "titulo": "Apto Vila Prudente",
    "valor": "270.000,00"
});
db.getCollection("imoveis").insert({
    "_id": ObjectId("5899c4a69835dae51a000002"),
    "autoIndexId": "true",
    "disponibilidade": "01/01/2017",
    "descricao": "Ótima localização, excelente imóvel, living, oportunidade de bom negócio, vale a pena agendar uma visita com um corretor Lopes Prime, venha morar bem com conforto e segurança. Conheça essa belo Casa de 220,00m com 3 dormitórios , 4 banheiros e 1 vaga para compra.",
    "email_contato": "email@email.com",
    "imagem": "http://multimidia.lopes.com.br/293/353-IM29528/casa-vila-madalena-sao-paulo-imagem-29528ebf0abb595f57f0449097e6671458b936.PNG",
    "telefone": "(11) 998236745",
    "titulo": "Apto Vila Madalena",
    "valor": "1.020.000,00"
});
db.getCollection("imoveis").insert({
    "_id": ObjectId("5899c5559835da89e0000004"),
    "autoIndexId": "true",
    "disponibilidade": "01/01/2017",
    "descricao": "Uma bela residência antiga na Rua Joaquim Távora, Vila Mariana, está aguardando um novo morador",
    "email": "email@email.com",
    "imagem": "http://www.saopauloantiga.com.br/wp-content/uploads/2013/10/ruajoaquimtavora1484x.jpg",
    "telefone": "(11) 998236745",
    "titulo": "Casa – Rua Joaquim Távora, 1484",
    "valor": "1.500.000,00"
});