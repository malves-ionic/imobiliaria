//Inicio o modulo de controle
angular.module('starter.controllers', [])

//Controle padrao da app
.controller('AppCtrl', function($scope, $state) {

})

//Controle para lista de imoveis
.controller('ImoveisCtrl', function($log, $rootScope, $stateParams, $scope, SweetHomeService) {
    var LOG = $log;
    //Campo de busca de imoveis
    $scope.textoBusca = "";

    //Realizo a limpeza do campo e busco de novo
    $scope.limparBusca = function() {
        $scope.textoBusca = "";
        $scope.imoveis = ImoveisService.get();
    }

    //Realizo a busca passando o texto da busca
    $scope.buscar = function() {
        $scope.imoveis = ImoveisService.get({ filtro: $scope.textoBusca });
    }

    //Caso nao seja selecionado a busca este sera o metodo padrao de busca
    SweetHomeService.$loaded()
        .then(function(x) {
            $scope.imoveis = SweetHomeService;
            //LOG.info('Imóveis:', $scope.imoveis);
        })
        .catch(function(error) {
            console.log("Error:", error);
        });

})

//Controle referente ao detalhe do imovel
.controller('ImovelCtrl', function($log, $scope, $stateParams, SweetHomeService) {
    var LOG = $log;
    //Metodo da busca remoto de imovel padrao
    $scope.imovel = SweetHomeService.$getRecord($stateParams.id + "");
})

.controller('CadastroCtrl', function($log, $scope, $state, SweetHomeService) {
    var LOG = $log;
    $scope.imovel = {}; //You can instantiate resource class
    $scope.cidades = ['Diadema', 'São Caetano do Sul', 'Santo André', 'São Paulo', 'São Bernado do Campo'];

    $scope.salvar = function(frm) {
        //LOG.info('Frm:', frm, 'Imóvel:', $scope.imovel);
        if (frm.$invalid) return;
        //var id =  Date.now() + "";
        SweetHomeService.$add($scope.imovel).then(function(ref) {
            var id = ref.key();
            //LOG.info("added record with id " + id);
            //SweetHomeService.$indexFor(id); // returns location in the array
            $state.go('app.imovel', { id: id });
        });
    }
});