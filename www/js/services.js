//Inicio o modulo de servico utilizando o ngResource para as chamadas http
angular.module('starter.services', [])

  .factory("SweetHomeService", function($firebaseArray, apiUrl) {
     var itemsRef = new Firebase(apiUrl.IMOVEIS);
     return $firebaseArray(itemsRef);
  })
