angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'firebase', 'ngMessages', 'ngAnimate'])

//Url constant used in services.
.constant("apiUrl", (function() {
    //API from firebaseio
    var resource = 'https://imobiliaria-1beaa.firebaseio.com/';
    return {
        IMOVEIS: resource + "imoveis"
    };
})())

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }

        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

//Configuring routes.
.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
    //Initial route
    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })

    //Route to home page.
    .state('app.home', {
        url: '/home',
        views: {
            'menuContent': {
                templateUrl: 'templates/home.html'
            }
        }
    })

    //Route to list of real estate.
    .state('app.imoveis', {
        url: '/imoveis',
        views: {
            'menuContent': {
                templateUrl: 'templates/imoveis.html',
                controller: 'ImoveisCtrl'
            }
        }
    })

    //Route detail from a real estate from the list.
    .state('app.imovel', {
            url: '/imovel/:id',
            views: {
                'menuContent': {
                    templateUrl: 'templates/imovel.html',
                    controller: 'ImovelCtrl'
                }
            }
        })
        //Route to registration of real estate.
        .state('app.cadastro', {
            url: '/cadastro',
            views: {
                'menuContent': {
                    templateUrl: 'templates/cadastro.html',
                    controller: 'CadastroCtrl'
                }
            }
        });
    //When entering the application or any error redirects for this route.
    $urlRouterProvider.otherwise('/app/home');
});