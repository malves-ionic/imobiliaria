# SWEET-HOME project

## Goal

> This application has as main feature allow transactions with real estate.

## Requirements

> Nodejs.
> ionic framework.
> firebase api, noSQL database.

## Build app for local development

> npm install

~~~javascript
> npm install
~~~

## Run App on browser

~~~javascript
> ionic server
~~~

## Url app

[localhost::8100](localhost:8100/)

## Contact

- Developer: Malves, 2017
- Contact: E-mail [malves.dev@gmail.com](<malves.dev@gmail.com>)
